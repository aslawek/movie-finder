import React, {Component} from 'react';
import Movie from './Movie';
import Header from './Header';

// import data from '../data.json';

class Landing extends Component {

    state = {
      searchTerm: ""
    };

    inputHandler = event => {
        this.setState({searchTerm: event.target.value})
    };

    render(){
        return (
            <div className="search">
                <Header showSearch inputHandler={this.inputHandler}/>
                <div>
                    {this.props.shows
                        .filter(item => `${item.title} ${item.description}`.toUpperCase().indexOf(this.state.searchTerm.toUpperCase()) >= 0)
                        .map(item => <Movie {...item} key={item.imdbID}/>)}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    searchTerm: state.searchTerm
});

export default Landing;