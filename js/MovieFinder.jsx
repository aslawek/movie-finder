import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Landing from './Landing';
import NotFound from './NotFound';
import Homepage from './Homepage';
import MovieDetails from './MovieDetails';
import data from '../data.json';

const App = () => (
    <BrowserRouter>
        <div className="app">
            <Switch>
                <Route exact path="/" component={Homepage}/>
                <Route path="/search" component={(props) => <Landing shows={data.shows} {...props}/>}/>
                <Route path="/details/:id" component={(props) => <MovieDetails movieDetails={data.shows.find(item => item.imdbID === props.match.params.id)} />}/>
                <Route component={NotFound}/>
            </Switch>
        </div>
    </BrowserRouter>
);

render(<App />, document.getElementById('app'));