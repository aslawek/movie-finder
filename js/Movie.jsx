import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
// this is for AirBnB styleguide (not React)
import { string } from 'prop-types';

const Wrapper = styled(Link)`
    width: 32%;
    border: 0.125rem solid #333;
    border-radius: 0.25rem;
    margin-bottom: 1.5625rem;
    padding-right: 0.675rem;
    overflow: hidden;
    color: black;
    text-decoration: none;
`;

const Image = styled.img`
    width: 46%;
    float: left;
    margin-right: 0.625rem;
`;

const Movie = props => (
    <Wrapper to={`/details/${props.imdbID}`} data-testid="movie">
        <Image src={`/public/img/posters/${props.poster}`} alt={`${props.title} Cover`}/>
        <div>
            <h3>{props.title}</h3>
            <h4>{props.year}</h4>
            <p>{props.description}</p>
        </div>
    </Wrapper>
);

// we need to tell React which props are required and their types
Movie.propTypes = {
    poster: string.isRequired,
    title: string.isRequired,
    year: string.isRequired,
    description: string.isRequired,
    imdbID: string.isRequired
};

export default Movie;