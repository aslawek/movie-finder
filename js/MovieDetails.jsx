import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
// import {string} from 'prop-types';
import Spinner from './Spinner';
import Header from './Header';

class MovieDetails extends React.Component {

    state = {
        rating: ''
    };

    componentDidMount(){
        axios.get(`http://localhost:3000/${this.props.movieDetails.imdbID}`)
            .then(response => {
                this.setState(response.data);
            })
        //window
        //event listener
        //3rd party libraries d3, jquery
        //AJAX
    }

    render(){
        let isLoaded;
        if(this.state.rating !== '') {
            isLoaded = <h3>rating {this.state.rating}</h3>
        } else {
            isLoaded = <Spinner/>
        }

        return (
            <div className="details">
                <Header/>
                <section>
                    <h1>{this.props.movieDetails.title}</h1>
                    <h2>{this.props.movieDetails.year}</h2>
                    <img src={`/public/img/posters/${this.props.movieDetails.poster}`} alt={`${this.props.title} Cover`}/>
                    {isLoaded}
                    <p>{this.props.movieDetails.description}</p>
                </section>
                <div>
                    <iframe src={`https://www.youtube-nocookie.com/embed/${this.props.movieDetails.trailer}?rel=0&amp;controls=0&amp;showinfo=0`}></iframe>
                </div>
            </div>
        )
    }
}

// MovieDetails.propType = {
//     poster: string.isRequired,
//     title: string.isRequired,
//     year: string.isRequired,
//     description: string.isRequired,
// };

export default MovieDetails;