import React from 'react'
import { Link } from 'react-router-dom';

class Header extends React.Component {
    render(){
        let uuk;
        if (this.props.showSearch) {
            uuk = <input onChange={this.props.inputHandler}
                         type="text"
                         value={this.props.searchTerm}
                         placeholder="Search"/>
        } else {
            uuk = <h2><Link to={"/search"}>Back</Link></h2>
        }

        return (
            <header>
                <h1>
                    <Link to={"/"}>Video Search</Link>
                </h1>
                {uuk}
            </header>
        )
    };
}


Header.defaultProps = {
    showSearch: false
};

export default Header;