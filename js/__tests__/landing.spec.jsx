import React from 'react';
import { shallow, mount } from 'enzyme';
import {render, fireEvent, wait } from 'react-testing-library';
import renderer from 'react-test-renderer';
import { Link, MemoryRouter} from 'react-router-dom';

import data from '../../data.json';
import Landing from '../Landing';
import Movie from '../Movie';

// NOT WORKING (should)
// test('Search should find 1 movie by phrase', () => {
//     // Arrange
//     const {container, getByPlaceholderText, getByTestId} = render(
//         <MemoryRouter>
//             <Landing shows={data.shows}/>
//         </MemoryRouter>
//     );
//
//     const phrase = "black";
//
//     // Act
//     const test = getAllByTestId('movie');
//     fireEvent.change(getByPlaceholderText('Search'), {target: {value: phrase}});
//
//     // Assert
//     expect(getAllByTestId('movie').length.toEqual(2))
// });

test('Search should be rendered', () => {
    // 1. Arange - zbieramy dane (renderuje komponent)
    const component = shallow(<Landing shows={data.shows}/>);
    // 2. Act - wykonujemy operację //
    // 3. Assert - sprawdzamy warunki //
    expect(component).toMatchSnapshot();
});

test('Landing should show all shows from data', () => {
    // Arrange
    const component = shallow(<Landing shows={data.shows}/>);
    const dataCounter = data.shows.length;
    // Act
    // Assert
    expect(component.find(Movie).length).toEqual(dataCounter);
});

test('Filter should return exact number of results (Movies)', () => {
    // Arrange
    const results = (phrase) => {
        return data.shows.filter(item => `${item.title} ${item.description}`.toUpperCase().indexOf(phrase.toUpperCase()) >= 0)
    };
    // Act
    // Assert
    expect(results('black').length).toEqual(2);
    expect(results('dupa').length).toEqual(0);
    expect(results('game').length).toEqual(1);
});

test('Landing should be rendered deeply', () => {
    // A
    const component = renderer.create(
        <MemoryRouter>
            <Landing shows={data.shows}/>
        </MemoryRouter>
    );
    // A
    // A
    expect(component).toMatchSnapshot();
});

// Syntax describe
// describe('Search', () => {
//     it('test for case 1', () => {
//         return true
//     });
//     it('test for case 2', () => {
//         return true
//     })
// });

/*
shallow - testuje tylko dany komponent (renderuje), bez dzieci
render - testuje komponent wraz z dziećmi (trzeba przekazać, razem z wartościami)
 */